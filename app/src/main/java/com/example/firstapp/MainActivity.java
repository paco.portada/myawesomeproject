package com.example.firstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button boton;
    Button boton2;
    TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boton = (Button) findViewById(R.id.button);
        //boton2 = (Button) findViewById(R.id.button2);
        texto = (TextView) findViewById(R.id.textView);
        boton.setOnClickListener(this);
        actualizar();
        boton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actualizar();
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == boton) {
            actualizar();
        }
    }

    private void actualizar() {
        texto.setText(new Date().toString());
    }

    public void actualizarHora(View view) {
        actualizar();
    }

    public void prueba() {
        texto.setText("un ejemplo");
    }
}